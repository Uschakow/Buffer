#include "RingBuffer.h"
#include <windows.h>
#include <cstdio>
using std::cout;
using std::endl;

template<class T>
std::mutex RingBuffer<T>::_mutex;
template<class T>
std::mutex RingBuffer<T>::_indexMutex;


template<class T>
inline RingBuffer<T>::RingBuffer(unsigned int size) : _bufferSize(size)
{
	std::lock_guard<std::mutex> lock(_mutex);
	AllocConsole();
	freopen("CONOUT$", "w", stdout); freopen("CONOUT$", "w", stdout);
	
#ifdef __DEUBG_
	cout << "Creating buffer with maximal capacity of " << size << " elements." << endl;
	cout << "Thread id: " <<std::this_thread::get_id() << endl;
#endif

	_ringBuffer = new T*[size];
	for (size_t i = 0; i < _bufferSize; i++)
	{
		_ringBuffer[i] = nullptr;
	}
}

template<class T>
inline RingBuffer<T>::~RingBuffer()
{
	std::lock_guard<std::mutex> lock(_mutex);
#ifdef __DEBUG_
	cout << "Buffer destroyed." << endl;
#endif
	for (size_t i = 0; i < _bufferSize; i++)
	{
		delete _ringBuffer[i];
		_ringBuffer[i] = nullptr;
	}
	delete[] _ringBuffer;
}

template<class T>
inline bool RingBuffer<T>::putItem(T * const newItem)
{
	std::lock_guard<std::mutex> lock(_mutex);
#ifdef __DEBUG_
	cout << "Buffer: Add element." << endl;
#endif
	_ringBuffer[_currentIndex] = newItem;
	_numberOfItems = _numberOfItems < _bufferSize ? ++_numberOfItems : _bufferSize;
	_generateNextIndex();
	return false;
}

template<class T>
inline T * RingBuffer<T>::getLastItem()
{
	std::lock_guard<std::mutex> lock(_mutex);
#ifdef __DEBUG_
	cout << "Buffer: removing last element." << endl;
#endif
	if (_numberOfItems == 0)
	{
#ifdef __DEBUG_
		cout << "Buffer: No elements in buffer. Returning nullpointer." << endl;
#endif
		return nullptr;
	}
	_generatePreviousIndex();
	_numberOfItems = _numberOfItems >= 0 ? --_numberOfItems : 0;
	T *item = _ringBuffer[_currentIndex];
	_ringBuffer[_currentIndex] = nullptr;
	return item;
}

template<class T>
inline unsigned int RingBuffer<T>::getNumberOfItems() const
{
	return _numberOfItems;
}

template<class T>
inline T** RingBuffer<T>::getHistoricalItems(const unsigned int numberOfItems)
{
	std::lock_guard<std::mutex> lock(_mutex);

	return NULL;
}

template<class T>
inline const void RingBuffer<T>::printStructure()
{
	std::lock_guard<std::mutex> lock(_mutex);
	std::cout << "Buffersize: \t" << _bufferSize << std::endl << "Elements in buffer: \t" << _numberOfItems << " -> " << (float)_numberOfItems / (float)_bufferSize * 100 << "% filled" << std::endl;
	for (size_t i = 0; i < _bufferSize; i++)
	{
		std::cout << "cell " << i << (_ringBuffer[i] == nullptr ? " empty" : " full") << std::endl;
	}
	std::cout << "=====" << std::endl;
}

template<class T>
inline void RingBuffer<T>::_generateNextIndex()
{
	std::lock_guard<std::mutex> lock(_indexMutex);
	_currentIndex = (++_currentIndex) % _bufferSize;
#ifdef __DEBUG_
	cout << "Buffer: Next index generated: " << _currentIndex << endl;
#endif
}

template<class T>
inline void RingBuffer<T>::_generatePreviousIndex()
{
	std::lock_guard<std::mutex> lock(_indexMutex);
	_currentIndex = _currentIndex == 0 ? _bufferSize - 1 : --_currentIndex;
	//_currentIndex = (--_currentIndex) % _bufferSize;
#ifdef __DEBUG_
	cout << "Buffer: Previous index generated: " << _currentIndex << endl;
#endif
}

template<class T>
inline void RingBuffer<T>::_generateNextIndex(unsigned int nIndex)
{
	std::lock_guard<std::mutex>(_indexMutex);
	_currentIndex = (_currentIndex + nIndex) % _bufferSize;
}

template<class T>
inline void RingBuffer<T>::_generatePrevIndex(unsigned int nIndex)
{

}
