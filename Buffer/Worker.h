#pragma once
#include <chrono>
#include <ctime>
#include <thread>
#include <iostream>
#include <mutex>

#include "BufferInterface.h"
#include "Dummy.h"


template <class T>
class Worker
{
public:
	Worker(BufferInterface<T> *buffer, void (*f)());
	~Worker();
	bool startWork(std::promise<bool>& prom);
	void stopWork();
	void printState();

private:
	BufferInterface<T> *_buffer;
	volatile bool _isWorking = false;
	unsigned int _maxSleep = 200;
	unsigned int _nPut = 0;
	unsigned int _nGet = 0;
	unsigned int _nMisses = 0;

	void _fillBuffer();
	void _emptyBuffer();
	static std::mutex _mutex;
	static std::mutex _workMutex;
	std::function<void ()> stopCallback;  //tell creator class when you stopped working.

	std::thread::id _id;
};


#include "Worker.inl"