#pragma once

#include <iostream>
#include <string>

class Dummy
{
public:
	Dummy();
	~Dummy();
private:
	static size_t _nElements;
	std::string _myData;
};

