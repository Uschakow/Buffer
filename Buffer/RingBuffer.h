#ifndef __RINGBUFFER__H__
#define	__RINGBUFFER__H__

#include <mutex>
#include <iostream>

#include "BufferInterface.h"

template <class T>
class RingBuffer :
	public BufferInterface<T>
{
public:
	RingBuffer(unsigned int size);
	~RingBuffer();
	bool putItem(T* const newItem);
	T* getLastItem();
	unsigned int getNumberOfItems() const;
	T** getHistoricalItems(const unsigned int numberOfItems);
	const void printStructure();

protected:
	static std::mutex _mutex;
	static std::mutex _indexMutex;
	T** _ringBuffer = nullptr;
	volatile unsigned int _numberOfItems = 0;
	volatile int _currentIndex = 0;
	const size_t _bufferSize;

	void _generateNextIndex();
	void _generatePreviousIndex();

	void _generateNextIndex(unsigned int nIndex);
	void _generatePrevIndex(unsigned int nIndex);

	RingBuffer(RingBuffer& ringBuffer);
};

#include "RingBuffer.inl"

#endif
