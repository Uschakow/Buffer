#include <iostream>
#include <future>
#include <functional>

#define __DEBUG_

#include "BufferInterface.h"
#include "RingBuffer.h"
#include "Worker.h"
#include "Dummy.h"

BufferInterface<int> *buffer = nullptr;
int commandProcess(char &command);
int nWorkerStopped = 0;

void stopWorker()
{
	nWorkerStopped++;
}

int main()
{
	buffer = new RingBuffer<int>(5);
	char command = '.';
	while (commandProcess(command))
		std::cin >> command;
	
	AllocConsole();
	freopen("CONOUT$", "w", stdout); freopen("CONOUT$", "w", stdout);
	for (int i = 0; i < 10; i++)
	{
		srand(time(0));
		BufferInterface<Dummy> *buffer = new RingBuffer<Dummy>(5);
		Worker<Dummy> *worker1 = new Worker<Dummy>(buffer, &stopWorker);
		Worker<Dummy> *worker2 = new Worker<Dummy>(buffer, &stopWorker);

		std::promise<bool> promise1;
		std::promise<bool> promise2;

		std::thread *t1 = new std::thread(&Worker<Dummy>::startWork, worker1, std::ref(promise1));

		//std::this_thread::sleep_for(std::chrono::microseconds(100));

		std::thread *t2 = new std::thread(&Worker<Dummy>::startWork, worker2, std::ref(promise2));

		t1->detach();
		t2->detach(); 
		
		bool isRunning = true;
		//while(isRunning)
			//std::this_thread::sleep_for(std::chrono::milliseconds(200));

		int timeSpan = 300;
		auto start = std::chrono::high_resolution_clock::now();
		auto stop = std::chrono::high_resolution_clock::now();
		while ((std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count() - timeSpan) > 0)
			stop = std::chrono::high_resolution_clock::now();

		worker1->stopWork();
		worker2->stopWork();

		std::future<bool> future1 = promise1.get_future();
		std::future<bool> future2 = promise2.get_future();

		auto f1 = future1.get();
		auto f2 = future2.get();

		delete t1;
		delete t2;
		t1 = nullptr;
		t2 = nullptr;

		worker1->printState();
		worker2->printState();

		delete worker1;
		delete worker2;
		worker1 = nullptr;
		worker2 = nullptr;
		
		buffer->printStructure();

		delete buffer;
		buffer = nullptr;
				
		nWorkerStopped = 0;
		std::cout << "Run " << i+1 << " completed." << endl;
	}
	
	return 0;
}


/*!
@return 0	If programm should stop
*/
int commandProcess(char &command)
{
	switch (command)
	{
	case 'a': //add new item
	{
		int *i = new int(1);
		buffer->putItem(i);
		break;
	}
	case 'g': //get last item
		delete buffer->getLastItem();
		break;
	case 'p': //print buffer
		buffer->printStructure();
		break;
	case 'h': //help
		cout << "a\t\tadd item" << endl << "g\t\tget item and delete it" << endl << "p\t\tprint buffer structure" << endl << "q\t\tquit program" << endl;
		break;
	case 'q': //quit
		return false;
	}
	return true;
}