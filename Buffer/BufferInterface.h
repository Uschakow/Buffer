#ifndef __BUFFERINTERFACE__H__
#define __BUFFERINTERFACE__H__


/*!
@class BufferInterface

@brief	This is a template interface to a buffer capable of storing pointers to items of any type.
		The size of the buffer is defined when constructing and can not be changed during the lifetime of the object implementing the interface.

		Note: To enable debug messages define __DEBUG_ before including BufferInterface.h
		
*/
template <class T>
class BufferInterface
{
public:
	virtual ~BufferInterface() {};

	
	/*!
	@brief	Adds a item to the buffer.

	@param[in] item	Pointer to the object being stored.
	
	@return			True if object could be inserted to the buffer.
	*/
	virtual bool putItem(T * const item) = 0;


	/*!
	@brief	Reads the last inserted element and returns it.

	@return	Last read element or nullptr if no element available to return.
	*/
	virtual T* getLastItem() = 0;


	/*!
	@brief	Returns the number of elements the buffer contains.

	@return	Returns the number of elements the buffer contains.
	*/
	virtual unsigned int getNumberOfItems() const = 0;


	/*!
	@brief	Returns numberOfElements elements from the buffer.

	@param[in]	numberOfElements	Number of elements to return. Must be <= of the return value of getNumberOfElements.

	@return		If numberOfElements is <= elements the buffer contains the method returns an array of pointers to the elemnts. The array is as long as numberOfElements.
				If numberOfElements could not be retreived this method will return a nullpointer.
	*/
	virtual T** getHistoricalItems(const unsigned int numberOfItems) = 0;


	/*!
	@brief		Prints its current state to cout.
	*/
	virtual const void printStructure() = 0;
protected:
	volatile unsigned int _numberOfItems = 0;
};

#endif