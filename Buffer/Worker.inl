#include <future>

#include "Worker.h"

using std::cout;
using std::endl;

template<class T>
std::mutex Worker<T>::_mutex;

template<class T>
std::mutex Worker<T>::_workMutex;

template <class T>
inline Worker<T>::Worker(BufferInterface<T> *buffer, void(*f)()) : _buffer(buffer)
{
	AllocConsole();
	freopen("CONOUT$", "w", stdout); freopen("CONOUT$", "w", stdout);
	std::lock_guard<std::mutex> lock(_mutex);
#ifdef __DEBUG_
	cout << "Worker created." << endl;
#endif
	stopCallback = f;
}


template <class T>
inline Worker<T>::~Worker()
{
	std::lock_guard<std::mutex> lock(_mutex);
}


template <class T>
inline bool Worker<T>::startWork(std::promise<bool> &prom)
{
	_id = std::this_thread::get_id();
#ifdef __DEBUG_
	cout << "Starting worker with id: " << _id << "." << endl;
#endif
	_isWorking = true;
	while (_isWorking)
	{
		srand(time(0));
		std::this_thread::sleep_for(std::chrono::milliseconds(rand() % _maxSleep));
		_workMutex.lock();
		//rand() % 2 == 0 ? (rand() % 2 == 0 ? _emptyBuffer() : _fillBuffer()): _fillBuffer();
		rand() % 2 == 0 ? _fillBuffer() : _emptyBuffer();
		_workMutex.unlock();
	}
	prom.set_value(true);
	return false;
}


template <class T>
inline void Worker<T>::stopWork()
{
	std::lock_guard<std::mutex> lock2(_workMutex);
	std::lock_guard<std::mutex> lock1(_mutex);
#ifdef __DEBUG_
	cout << "Stopping worker with thread id: " << _id << endl;
#endif
	_isWorking = false;
	stopCallback();
}


template <class T>
inline void Worker<T>::_fillBuffer()
{
	std::lock_guard<std::mutex> lock(_mutex);
#ifdef __DBUG_
	cout << "Worker " << _id << ": PUT." << endl;
#endif
	if (!_buffer)
		return;
	_buffer->putItem(new T());
	_nPut++;
}


template <class T>
inline void Worker<T>::_emptyBuffer()
{
	if (!_buffer)
		return;
	std::lock_guard<std::mutex> lock(_mutex);
#ifdef __DEBUG_
	cout << "Worker " << _id << ": GET." << endl;
#endif
	Dummy *dummy = _buffer->getLastItem();
	if (!dummy)
		_nMisses++;
	delete dummy;
	_nGet++;
}

template <class T>
inline void Worker<T>::printState()
{
	cout << "Worker:" << endl << "\tID: " << _id << endl << "\tPut: " << _nPut
		<< endl << "\tGet: " << _nGet << endl << "\tMissed: " << _nMisses << endl;
}